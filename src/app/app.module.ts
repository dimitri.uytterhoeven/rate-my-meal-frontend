import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParticlesModule } from 'angular-particle';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuModule } from './modules/menu/menu.module';
import { CardsModule } from './modules/cards/cards.module';
import { SharedModule } from './shared/shared.module';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';

@NgModule({
	declarations: [
	AppComponent
	],
	imports: [
	BrowserModule,
	FontAwesomeModule,
	AppRoutingModule,
	ParticlesModule,
	HttpClientModule,
	BrowserAnimationsModule,
	CardsModule,
	SharedModule,
	MenuModule,
	ToastrModule.forRoot({
		progressBar: true,
		tapToDismiss: true,
		newestOnTop: true,
		maxOpened: 5,
		timeOut: 3000,
		positionClass: 'toast-top-center',
		preventDuplicates: false,
	}) // ToastrModule added
	],
	providers: [
		{ provide: LocationStrategy, useClass: PathLocationStrategy },
		// checkEnv('mobile') ? { provide: CONFIGURATION_FILE, multi:true, useValue: 'assets/configuration/properties/mobile.properties' } : []
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
