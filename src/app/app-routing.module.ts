import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardListComponent } from './modules/cards/card-list/card-list.component';
import { CardDetailComponent } from './modules/cards/card-detail/card-detail.component';
import { CardCreateComponent } from './modules/cards/card-create/card-create.component';


const routes: Routes = [
	{
	path: '',
	redirectTo: '/card-list',
	pathMatch: 'full'
	},
	{
	path: '',
	children: [
		{
		path: 'card-list',
		component: CardListComponent,
		data: {animation: 'CardsPage'}
		},
		{
		path: 'card-detail/:id',
		component: CardDetailComponent,
		data: {animation: 'CardsPage'}
		},
		{
		path: 'card-new',
		component: CardCreateComponent,
		data: {animation: 'CardsPage'}
		},
	]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
