import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ApiResponse } from '../models/ApiResponse';
import { Meal } from '../models/Meal';

@Injectable({
	providedIn: 'root'
})
export class MealService {

	public currentId;

	constructor(private http: HttpClient) { }
	baseUrl = 'http://vps743766.ovh.net:8080/api/v1/meals/';

	listMeals(): Observable<ApiResponse> {
		return this.http.get<ApiResponse>(this.baseUrl);
	}

	getMealById(id: string): Observable<ApiResponse> {
		return this.http.get<ApiResponse>(this.baseUrl + id);
	}

	createMeal(meal: Meal): Observable<ApiResponse> {
		return this.http.post<ApiResponse>(this.baseUrl, meal);
	}

	updateMeal(meal: Meal): Observable<ApiResponse> {
		return this.http.put<ApiResponse>(this.baseUrl + meal.id, meal);
	}

	deleteMeal(id: string): Observable<ApiResponse> {
		if (this.currentId) {
			return this.http.delete<ApiResponse>(this.baseUrl + id);
		} else {
			return of();
		}
	}
}
