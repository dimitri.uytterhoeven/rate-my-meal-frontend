import { TestBed } from '@angular/core/testing';

import { MealService } from './meal.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('MealService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		declarations: [],
		imports: [

		],
		providers: [
			{ provide: HttpClient, useClass: HttpClient },
			{ provide: HttpHandler, useClass: HttpHandler }
		]
	}));

	it('should be created', () => {
		const service: MealService = TestBed.get(MealService);
		expect(service).toBeTruthy();
	});
});
