export class Meal {
	id: string;
	title: string;
	image: string;
	content: string;
	rate: number;
	createdAt: string;
	updatedAt: string;
}
