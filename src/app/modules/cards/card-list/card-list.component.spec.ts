import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardListComponent } from './card-list.component';
import { SharedModule } from '../../../shared/shared.module';
import { Router } from '@angular/router';
import { CardComponent } from '../card/card.component';
import { MealService } from '../../../core/services/meal.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('CardListComponent', () => {
	let component: CardListComponent;
	let fixture: ComponentFixture<CardListComponent>;

	beforeEach(async(() => {
	TestBed.configureTestingModule({
		declarations: [ CardListComponent, CardComponent ],
		imports: [
			SharedModule,
			BrowserModule,
			BrowserAnimationsModule,
		],
		providers: [
			{ provide: HttpClient, useClass: HttpClient },
			{ provide: HttpHandler, useClass: HttpHandler },
			{ provide: Router, useClass: class { navigate = jasmine.createSpy('navigate'); } },
			MealService
		]
	})
	.compileComponents();
	}));

	beforeEach(() => {
	fixture = TestBed.createComponent(CardListComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
	});

	it('should create', () => {
	expect(component).toBeTruthy();
	});
});
