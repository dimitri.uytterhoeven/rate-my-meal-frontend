import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Meal } from '../../../core/models/Meal';
import { MealService } from '../../../core/services/meal.service'; import {
	style, animate, animation, animateChild,
	useAnimation, group, sequence, transition, state, trigger, stagger, query
} from '@angular/animations';

@Component({
	selector: 'app-card-list',
	templateUrl: './card-list.component.html',
	styleUrls: ['./card-list.component.scss'],
	animations: [
		// more info @
		// https://medium.com/google-developer-experts/angular-applying-motion-principles-to-a-list-d5cdd35c899e
		// and other one
		// https://medium.com/@tejozarkar/angular-stagger-animation-cards-cascading-fe45eb003759
		// nice stagger effect when showing existing elements
		trigger('list', [
			transition(':enter', [
				// child animation selector + stagger
				query('@items',
					stagger('150ms', animateChild()),
					{ optional: true }
				)
			]),
		]),
		trigger('items', [
			// cubic-bezier for a tiny bouncing feel
			transition(':enter', [
				style({ transform: 'scale(0.5)', opacity: 0 }),
				animate('.5s cubic-bezier(.8,-0.6,0.2,1.5)',
					style({ transform: 'scale(1)', opacity: 1 }))
			]),
			transition(':leave', [
				style({ transform: 'scale(1)', opacity: 1, height: '*' }),
				animate('.5s cubic-bezier(.8,-0.6,0.2,1.5)',
					style({ transform: 'scale(0.5)', opacity: 0, height: '0px', margin: '0px' }))
			]),
		])
	],
})
export class CardListComponent implements OnInit {

	meals: Meal[];

	constructor(
		private router: Router,
		private mealService: MealService
	) { }

	ngOnInit() {
	this.listMeals();
	this.mealService.currentId = '';
	}

	navigate(cardId) {
	this.router.navigate([`/card-detail/${cardId}`]);
	}

	listMeals(): void {
	this.mealService.listMeals().subscribe((data) => {
		this.meals = data.goal;
		console.log('list meals', data.goal);
	}, (error) => {
		console.log(error);
	});
	}

}
