import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { CardListComponent } from './card-list/card-list.component';
import { SharedModule } from '../../shared/shared.module';
import { CardCreateComponent } from './card-create/card-create.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {
	HtmlEditorService,
	ImageService,
	LinkService, RichTextEditorAllModule,
	TableService,
	ToolbarService
} from '@syncfusion/ej2-angular-richtexteditor';
import { MatButtonModule, MatInputModule, MatSliderModule } from '@angular/material';
import { MealService } from '../../core/services/meal.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
	declarations: [
	CardComponent,
	CardDetailComponent,
	CardListComponent,
	CardCreateComponent
	],
	imports: [
		CommonModule,
		BrowserModule,
		BrowserAnimationsModule,
		SharedModule,
		ImageCropperModule,
		FontAwesomeModule,
		FormsModule,
		ReactiveFormsModule,
		RichTextEditorAllModule,
		MatSliderModule,
		MatInputModule,
		MatButtonModule
	],
	providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService, MealService]
})
export class CardsModule { }
