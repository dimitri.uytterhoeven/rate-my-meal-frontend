import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-card',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

	@Input()
	title: string;

	@Input()
	image: string;

	@Input()
	rate: string;

	constructor() { }

	ngOnInit() {
	}

	nameRate(rateString: string): string {
	const rate = Number(rateString);
	let name = '';
	switch (rate) {
		case 5:
		case 4.5:
		name = 'Amazing !!!';
		break;
		case 4:
		case 3.5:
		name = 'Delicious !!';
		break;
		case 3:
		case 2.5:
		name = 'Good !';
		break;
		case 2:
		case 1.5:
		name = 'Middle...';
		break;
		case 1:
		case 0.5:
		case 0:
		name = 'Awful..';
		break;
		default:
		name = '';
	}
	return name;
	}

}
