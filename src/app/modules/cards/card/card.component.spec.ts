import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { SharedModule } from '../../../shared/shared.module';

describe('CardComponent', () => {
	let component: CardComponent;
	let fixture: ComponentFixture<CardComponent>;

	beforeEach(async(() => {
	TestBed.configureTestingModule({
		declarations: [ CardComponent ],
		imports: [
			SharedModule
		]
	})
	.compileComponents();
	}));

	beforeEach(() => {
	fixture = TestBed.createComponent(CardComponent);
	component = fixture.componentInstance;
	component.title = '';
	component.image = '';
	component.rate = '';
	fixture.detectChanges();
	});

	it('should create', () => {
	expect(component).toBeTruthy();
	});
});
