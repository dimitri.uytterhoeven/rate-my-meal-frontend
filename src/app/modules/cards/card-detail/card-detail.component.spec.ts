import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDetailComponent } from './card-detail.component';
import { ActivatedRoute, Router } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MealService } from '../../../core/services/meal.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('CardDetailComponent', () => {
	let component: CardDetailComponent;
	let fixture: ComponentFixture<CardDetailComponent>;

	const fakeActivatedRoute = {
	snapshot: {data: {}, url: {}, params: [{id: '1'}]}
	} as unknown as ActivatedRoute;

	beforeEach(async(() => {
	TestBed.configureTestingModule({
		declarations: [ CardDetailComponent ],
		imports: [
			FontAwesomeModule,
			BrowserAnimationsModule
		],
		providers: [
			{ provide: HttpClient, useClass: HttpClient },
			{ provide: HttpHandler, useClass: HttpHandler },
			{ provide: ActivatedRoute, useValue: fakeActivatedRoute },
			{ provide: Router, useClass: class { navigate = jasmine.createSpy('navigate'); } },
			MealService
		]
	})
	.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CardDetailComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
