import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
	faArrowCircleLeft
} from '@fortawesome/free-solid-svg-icons';
import { Meal } from '../../../core/models/Meal';
import { MealService } from '../../../core/services/meal.service';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
		selector: 'app-card-detail',
		templateUrl: './card-detail.component.html',
		styleUrls: ['./card-detail.component.scss'],
	animations: [
		trigger('slideInOut', [
			transition(':enter', [
				style({transform: 'translateX(-100%)'}),
				animate('300ms ease-out', style({transform: 'translateX(0%)'}))
			]),
			transition(':leave', [
				animate('300ms ease-in', style({transform: 'translateX(-100%)'}))
			])
		]),
		trigger('fadeInOut', [
			transition(':enter', [
				style({opacity: 0}),
				animate('300ms ease-out', style({opacity: 1}))
			]),
			transition(':leave', [
				animate('300ms ease-in', style({opacity: 0}))
			])
		])
	]
})
export class CardDetailComponent implements OnInit {

	faArrowCircleLeft = faArrowCircleLeft;

	meal: Meal;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private mealService: MealService
	) { }

	ngOnInit() {
	this.getMealById(this.route.snapshot.params.id);
	this.mealService.currentId = this.route.snapshot.params.id;
	}

	getMealById(id: string): void {
	this.mealService.getMealById(id)
	.subscribe( data => {
		this.meal = data.goal;
	}, (error) => {
		console.log(error);
	});
	}

	navigateBack() {
	this.router.navigate(['/card-list']);
	}

}
