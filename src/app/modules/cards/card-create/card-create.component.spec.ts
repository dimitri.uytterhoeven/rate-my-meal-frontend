import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardCreateComponent } from './card-create.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
	HtmlEditorService,
	ImageService,
	LinkService, RichTextEditorAllModule,
	TableService,
	ToolbarService
} from '@syncfusion/ej2-angular-richtexteditor';
import { MatButtonModule, MatInputModule, MatSliderModule } from '@angular/material';
import { SharedModule } from '../../../shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CardComponent } from '../card/card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MealService } from '../../../core/services/meal.service';
import { Router } from '@angular/router';

describe('CardCreateComponent', () => {
	let component: CardCreateComponent;
	let fixture: ComponentFixture<CardCreateComponent>;

	beforeEach(async(() => {
	TestBed.configureTestingModule({
		declarations: [ CardCreateComponent, CardComponent ],
		imports: [
		BrowserAnimationsModule,
		SharedModule,
		ImageCropperModule,
		FontAwesomeModule,
		FormsModule,
		ReactiveFormsModule,
		RichTextEditorAllModule,
		MatSliderModule,
		MatInputModule,
		MatButtonModule
		],
		providers: [
		{ provide: HttpClient, useClass: HttpClient },
		{ provide: HttpHandler, useClass: HttpHandler },
		{ provide: Router, useClass: class { navigate = jasmine.createSpy('navigate'); } },
		MealService,
		ToolbarService, LinkService, ImageService, HtmlEditorService, TableService
		]
	})
	.compileComponents();
	}));

	beforeEach(() => {
	fixture = TestBed.createComponent(CardCreateComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
	});

	it('should create', () => {
	expect(component).toBeTruthy();
	});
});
