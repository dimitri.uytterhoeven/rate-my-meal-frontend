import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MealService } from '../../../core/services/meal.service';
import { Meal } from '../../../core/models/Meal';
import { Router } from '@angular/router';

@Component({
	selector: 'app-card-create',
	templateUrl: './card-create.component.html',
	styleUrls: ['./card-create.component.scss']
})
export class CardCreateComponent implements OnInit {

	imageChangedEvent: any = '';
	croppedImage: any = '';

	tools: object = {
	items: [
		'Bold', 'Italic', '|',
		'OrderedList', 'UnorderedList', '|',
		'Formats', '|',
		'LowerCase', 'UpperCase', '|',
		'Undo', 'Redo'
	]
	};

	cardForm: FormGroup;

	constructor(
		private fb: FormBuilder,
		private mealService: MealService,
		private router: Router
	) {
	this.createForm();
	}

	ngOnInit() {
	}


	createForm() {
	this.cardForm = this.fb.group({
		title: ['', Validators.required],
		rate: ['1.5', Validators.required],
		image: ['', Validators.required],
		content: ['']
	});
	}

	createMeal(): void {
	this.mealService.createMeal(this.cardForm.value)
		.subscribe( data => {
		this.router.navigate(['/card-list']);
	}, (error) => {
		console.log(error);
	});
	}
	disableValidate(): boolean {
	return !this.cardForm.valid;
	}

	fileChangeEvent(event: any): void {
	this.imageChangedEvent = event;
	}
	imageCropped(event: ImageCroppedEvent) {
	this.croppedImage = event.base64;
	const image = 'image';
	this.cardForm.controls[image].setValue(this.croppedImage);
	}
	imageLoaded() {
	// show cropper
	}
	cropperReady() {
	// cropper ready
	}
	loadImageFailed() {
	// show message
	}

}
