import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuComponent } from './menu.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from '../../shared/shared.module';
import { Router } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('MenuComponent', () => {
	let component: MenuComponent;
	let fixture: ComponentFixture<MenuComponent>;

	beforeEach(async(() => {
	TestBed.configureTestingModule({
		declarations: [ MenuComponent ],
		imports: [
			FontAwesomeModule,
			SharedModule
		],
		providers: [
			{ provide: Router, useClass: class { navigate = jasmine.createSpy('navigate'); } },
			{ provide: HttpClient, useClass: HttpClient },
			{ provide: HttpHandler, useClass: HttpHandler }
		]
	})
	.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MenuComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
