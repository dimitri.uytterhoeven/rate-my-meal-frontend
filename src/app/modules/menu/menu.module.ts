import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';



@NgModule({
	declarations: [MenuComponent],
	exports: [
	MenuComponent
	],
	imports: [
	CommonModule,
	FontAwesomeModule,
	SharedModule,
	RouterModule
	]
})
export class MenuModule { }
