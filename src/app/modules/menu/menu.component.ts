import { Component, OnInit } from '@angular/core';
import {
	faUser,
	faCog,
	faComment,
	faTrash,
	faBatteryHalf,
	faCalendar,
	faCloud,
	faWifi,
	faEnvelope,
	faPaperPlane,
	faHome,
	faPencilAlt
} from '@fortawesome/free-solid-svg-icons';
import 'hammerjs';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { MealService } from '../../core/services/meal.service';

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss'],
	animations: [
	trigger('changeState', [
		state('state0', style({
		})),
		state('state1', style({
		transform: 'rotate(116deg)'
		})),
		state('state2', style({
		transform: 'rotate(116deg)'
		})),
		state('state3', style({
		transform: 'rotate(116deg)'
		})),
		transition('*=>state0', animate('1110ms')),
		transition('*=>state1', animate('1110ms')),
		transition('*=>state2', animate('1110ms')),
		transition('*=>state3', animate('1110ms'))
	])
	]
})
export class MenuComponent implements OnInit {

	currentState = 'state0';

	faUser = faUser;
	faCog = faCog;
	faComment = faComment;
	faTrash = faTrash;
	faBattery = faBatteryHalf;
	faCalendar = faCalendar;
	faCloud = faCloud;
	faWifi = faWifi;
	faEnvelope = faEnvelope;
	faPaperPlane = faPaperPlane;
	faPencilAlt = faPencilAlt;
	faHome = faHome;

	// constant for swipe action: left or right
	SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };

	links = [
	{ icon: faUser, destination: '/card-list', action: '' },
	{ icon: faCog, destination: '/card-list', action: '' },
	{ icon: faPaperPlane, destination: '/card-list', action: '' },
	{ icon: faComment, destination: '/card-list', action: '' },
	{ icon: faTrash, destination: '/card-list', action: 'delete' },
	{ icon: faPencilAlt, destination: '/card-new', action: '' },
	{ icon: faHome, destination: '/card-list', action: '' },
	{ icon: faCloud, destination: '/card-list', action: '' },
	{ icon: faWifi, destination: '/card-list', action: '' },
	{ icon: faEnvelope, destination: '/card-list', action: ''  }
	];

	constructor(
		private router: Router,
		private mealService: MealService
	) { }

	ngOnInit() {
	}

	// action triggered when user swipes
	swipe(action = this.SWIPE_ACTION.LEFT) {
	// console.log(this.currentState);
		// swipe right, next menu icons
		if (action === this.SWIPE_ACTION.RIGHT) {
		switch (this.currentState) {
			case 'state1':
			this.changeState('state2');
			break;
			case 'state2':
			this.changeState('state3');
			break;
			case 'state3':
			this.changeState('state1');
			break;
			default:
			this.changeState('state1');
		}
		}

		// swipe left, previous menu icons
		if (action === this.SWIPE_ACTION.LEFT) {
			switch (this.currentState) {
			case 'state1':
				this.changeState('state3');
				break;
			case 'state2':
				this.changeState('state1');
				break;
			case 'state3':
				this.changeState('state2');
				break;
			default:
				this.changeState('state1');
			}
		}
	}

	changeState(nextState: any) {
	this.currentState = nextState;
	}

	navigate(destination: string) {
	// console.log(destination);
	setTimeout(() => this.router.navigate([destination]), 100);
	}

	nothing() {
		// do nothing
	}

	action(action) {
		switch (action) {
			case 'delete':
				this.mealService.deleteMeal(this.mealService.currentId)
					.subscribe( data => {
						// console.log('mealId delete', this.mealService.currentId);
					}, (error) => {
						console.log(error);
					}, () => {
						this.mealService.currentId = '';
					});
				break;
			default:
				break;
		}
	}
}
