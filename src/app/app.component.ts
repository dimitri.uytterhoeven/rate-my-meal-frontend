import { Component, OnInit } from '@angular/core';
import particlesConfig from 'src/assets/particlesjs-config.json';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	title = 'rate-my-meal';

	// Variables for particles.js
	myStyle: object = {};
	myParams: object = {};
	width = 100;
	height = 100;

	constructor() {
	}

	ngOnInit() {

	// Configuration and Param for particle.js
	this.myStyle = {
		position: 'fixed',
		width: '100%',
		height: '100%',
		'z-index': -2,
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
	};

	this.myParams = particlesConfig;

	}
}



