import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingComponent } from './rating.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('RatingComponent', () => {
	let component: RatingComponent;
	let fixture: ComponentFixture<RatingComponent>;

	beforeEach(async(() => {
	TestBed.configureTestingModule({
		declarations: [ RatingComponent ],
		imports: [
		FontAwesomeModule,
		]
	})
	.compileComponents();
	}));

	beforeEach(() => {
	fixture = TestBed.createComponent(RatingComponent);
	component = fixture.componentInstance;
	component.rate = '1';
	fixture.detectChanges();
	});

	it('should create', () => {
	expect(component).toBeTruthy();
	});
});
