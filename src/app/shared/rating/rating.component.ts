import { Component, Input, OnChanges, OnInit } from '@angular/core';
import {
	faStar,
	faStarHalfAlt
} from '@fortawesome/free-solid-svg-icons';
import {
	faStar as farStar
} from '@fortawesome/free-regular-svg-icons';

@Component({
	selector: 'app-rating',
	templateUrl: './rating.component.html',
	styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit, OnChanges {

	faStar = faStar;
	faStarHalfAlt = faStarHalfAlt;
	farStar = farStar;

	icons = [];
	nbrMaxStars = 5;

	@Input()
	rate: string;

	constructor() { }

	ngOnInit() {
	}

	ngOnChanges() {
	this.addIconsStrategy();
	}

	addIconsStrategy() {
		this.icons = [];

		const entier = Math.floor(Number(this.rate));
		const decimal = Number(this.rate) % 1;

		for (let i = 0; i < entier; i++) {
			this.icons.push( { icon: faStar, emptyStar: false } );
		}

		if (entier < 5 && decimal !== 0) {
			this.icons.push( { icon: faStarHalfAlt, emptyStar: false } );
		}

		if (this.nbrMaxStars - entier >= 1 && decimal === 0) {
			for (let j = 0; j < this.nbrMaxStars - entier; j++) {
				this.icons.push( { icon: farStar, emptyStar: false } );
			}
		} else if (this.nbrMaxStars - entier >= 1 && decimal !== 0) {
			for (let j = 0; j < this.nbrMaxStars - entier - 1; j++) {
				this.icons.push( { icon: farStar, emptyStar: false } );
			}
		}
	}

}
