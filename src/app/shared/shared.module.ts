import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PipesModule } from './pipes/pipes.module';
import { RatingComponent } from './rating/rating.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { StopEventPropagationDirective } from './directives/stop-event-propagation.directive';

@NgModule({
	imports: [
	CommonModule,
	PipesModule,
	FontAwesomeModule,
],
	declarations: [RatingComponent, StopEventPropagationDirective],
	exports: [
	CommonModule,
	PipesModule,
	RatingComponent,
	StopEventPropagationDirective
	]
})
export class SharedModule {}
