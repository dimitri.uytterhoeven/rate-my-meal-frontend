FROM nginx:1.16.0-alpine

WORKDIR .

COPY ./healthcheck.js /

COPY ./dist/rate-my-meal /usr/share/nginx/html

EXPOSE 80

RUN apk add --update nodejs

HEALTHCHECK --interval=12s --timeout=12s --start-period=30s CMD node /healthcheck.js

CMD ["nginx", "-g", "daemon off;"]

