#!/bin/sh

export PROJECT_NAME='MobileApp'
export PROJECT_LOCATION=$(pwd)/$PROJECT_NAME
export WWW_DIR=$PROJECT_LOCATION/www
export APK_DIR=$PROJECT_LOCATION/platforms/android/build/outputs/apk
export CORDOVA_VERSION=$(cordova -v)
export CORDOVA_TARGET='7.1.0'
export BASE_DIR=$(pwd)

fullClean () {
    echo "Full clean : "
    echo "-------------"
    cleanProject $1
    cleanWWW
}

cleanProject () {
 	cd $BASE_DIR
    if [ -d $PROJECT_NAME ]; then
     	echo "Clean project : " $PROJECT_NAME " for platform " $2
    	echo "----------------"
        cd $PROJECT_NAME
        cordova clean $1
    else
    	echo "No project to clean"
    fi
}

cleanWWW () {
    if [ -d $WWW_DIR ]; then
     	echo "Clean project www : $WWW_DIR "
    	echo "--------------------"
        rm -rf $WWW_DIR/*
    else
    	echo "No mobile sources to clean"
    fi
}


installNodeDependencies () {
    echo "Project dependencies installation : "
    echo "------------------------------------"
    npm install
}

deleteProject () {
	cd $BASE_DIR
    if [ -d $PROJECT_NAME ]; then
     	echo "Project deletion : $PROJECT_NAME "
    	echo "-------------------"
        rm -rf $PROJECT_NAME
    else
    	echo "No project to delete"
    fi
}

deleteBinaries () {
	cd $BASE_DIR
    if [ -d $BASE_DIR/APK ]; then
     	echo "Binaries deletion : $APK_DIR "
    	echo "--------------------"
        rm -rf $APK_DIR/APK
    else
    	echo "No binaries to delete"
    fi
}

create () {
	cd $BASE_DIR
	if [ $CORDOVA_VERSION != $CORDOVA_TARGET  ]; then
        echo "Cordova is not installed, running npm install"
        npm install -g cordova@$CORDOVA_TARGET
    fi
    echo "Project creation : $PROJECT_NAME"
    echo "-------------------"
    if [ -d $PROJECT_NAME ]; then
        echo "Project already exists cleaning before create"
        deleteProject
    fi
    cordova create $PROJECT_NAME
    cp config.xml $PROJECT_NAME
	cp ./mobile_app/res/icon/favicon.png $PROJECT_NAME
}

addPlatform () {
	cd $BASE_DIR
    if [[ ! -d $PROJECT_NAME ]]; then
    	echo "Project was not created"
        create
    fi
    if [[ -d $PROJECT_NAME/platforms/$1 ]]; then
		echo "Platform $1 already exists..."
	else
		echo "Project platform creation : "$1
		echo "----------------------------"
		cd $PROJECT_NAME
		cordova platform add $1
	fi
}

copyWWW () {
	cd $BASE_DIR
    if [ -d $WWW_DIR ]; then
     	echo "Copy project www : "
    	echo "-------------------"
        rm -rf $WWW_DIR/*
        if [[ ! -d dist ]]; then
            rm -rf dist
        fi
        launchNGBuild
        cp -r ./dist/* $WWW_DIR
        # rm -f $WWW_DIR/*.gz
    fi
}

launchNGBuild () {
    if [ ! -d node_modules ]; then
        installNodeDependencies
    fi
    echo "Building angular project : "
    echo "---------------------------"
	ng b -sm --aot=false --base-href=file:///android_asset/www/ --environment=mobile
	# The environment mobile will set the mobile flag to true in the app.
	# Actually it will load the mobile.properties file.
	# But you can check this variable in the app with checkEnv('mobile') from @mdib/utils. (see app.module)
}

launchCordovaBuild () {
	cd $BASE_DIR
    if [[ ! -d $PROJECT_NAME ]]; then
        echo "$PROJECT_NAME doesn't exist, nothing to build !"
    else
        echo "Mobile project build : "
        echo "-----------------------"
        cd $PROJECT_NAME
        cordova build --status
    fi
}

# add IPA part ?
moveAPK () {
    if [[ ! -d $APK_DIR ]]; then
        echo "No APK files found at $APK_DIR"
    else
        if [[ ! -d APK ]]; then
            mkdir APK
        fi
        cp $APK_DIR/* APK/*
    fi
}

if [ $1 = "fullBuildAPK" ]; then
    deleteProject
    create
    addPlatform $2
    copyWWW
    launchCordovaBuild
    # moveAPK
elif [ $1 = "delete" ]; then
    deleteProject
    deleteBinaries
elif [ $1 = "updateAPK" ]; then
    cleanWWW
    launchNGBuild
    copyWWW
    launchCordovaBuild
    moveAPK
fi

# $SHELL # Uncomment to debug or see the log
