#!/bin/bash

### CONSTANTS ###

cordovaProjectDir=mobile_app
iosXcodeProjectArchiveFile=~/WebBanking_iOS_xcode_project.tar

### PARAMETERS ###

platform=$1
if [[ -z "$platform" ]];then
    echo "ERROR: You must provide the platform as first argument. [android/ios]"
    exit 1
fi

forceRecompileSources=1
if [[ "$2" == "no-recompile" ]];then
    forceRecompileSources=0
fi

skipSSLCertificateValidation=1

### MAIN SCRIPT ###

cd `dirname $0`
cd ../
rootDir=`pwd`
wwwDir=$rootDir/$cordovaProjectDir/www
platformDir=$rootDir/$cordovaProjectDir/platforms/$platform

if [[ $forceRecompileSources -gt 0 || "`ls $wwwDir 2>/dev/null |wc -l`" -eq "0" ]];then
	echo "===== Installing NPM dependencies ====="
	npm install
	git checkout package-lock.json

    echo "===== Adaptating the sources for the mobile app ==="

    # Replace the absolute references to /assets in the CSS url() by a relative reference
    egrep -R -l "url *\( *['\"]?/assets/" src |while read file
    do
        echo "(...removing absolute references to /assets in file $file)"
        sed -i '' "s~url *( *['\"]\{0,1\}/assets/\([^'\"]*\)['\"]\{0,1\})~url(assets/\\1)~g" $file
        cat $file |grep assets
    done

    echo "===== Building the sources for the mobile app ==="
    rm -rf $wwwDir
    ng build --configuration=mobile --output-path $wwwDir/ || exit 1
else
    echo "(Reusing previously built web sources)"
fi

echo "===== Building the $platform app ==="
rm -rf $platformDir
cd $cordovaProjectDir
cordovaProjectName=`cat config.xml |grep "<name" |sed "s~.*<name.*>\(.*\)</name>.*~\\1~g"`

cordova platform add $platform || exit 1
cordova build $platform --buildConfig || exit 1

if [[ $skipSSLCertificateValidation -eq 1 && "$platform" == "ios" ]];then
    echo "Disabling the SSL Certificates validation..."
    appDelegateMFile=`ls -1 $platformDir/*/Classes/AppDelegate.m`
    printf "\n@implementation NSURLRequest(DataController)" >>$appDelegateMFile
    printf "\n+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host" >>$appDelegateMFile
    printf "\n{ return YES; }" >>$appDelegateMFile
    printf "\n@end \n" >>$appDelegateMFile
fi

echo "The mobile app/project has been generated in the $platformDir folder."
if [[ "$platform" == "ios" ]];then
    echo "If you want to use it on a testing device, don't forget to enable the Developer mode in the Safari settings."
    cd $platformDir/../
    tar -cf $iosXcodeProjectArchiveFile $platform
    echo "A TAR containing the iOS app XCode project has been generated in $iosXcodeProjectArchiveFile"
    cd -
    open $platformDir/*.xcodeproj
fi

exit 0
