@echo off

set PROJECT_NAME=MobileApp
set WWW_DIR=%PROJECT_NAME%\www
set APK_DIR=%PROJECT_NAME%\platforms\android\build\outputs\apk
set BASE_DIR=%CD%
set CORDOVA_BIN=%CD%\node_modules\cordova\bin

for %%x in (%*) do (
    if %%x equ fullBuildAPK (
        call :DELETE_PROJECT
        call :DELETE_BINARIES
        call :CREATE
        call :ADD_PLATFORM_ANDROID
        call :COPY_WWW
        call :BUILD
        call :MOVE_BINARIES
    ) else if %%x equ delete (
        call :DELETE_PROJECT
        call :DELETE_BINARIES
    ) else if %%x equ updateAPK (
        call :CLEAN_WWW
        call :LAUNCH_NG_BUILD
        call :COPY_WWW
        call :BUILD
        call :MOVE_BINARIES
    ) else (
        call :DISPLAY_HELP
    )
)

goto :EOF

REM ----------------------------------------- Subroutines -----------------------------------------


REM ---------------------------- CREATE ----------------------------
REM create cordova project (if it doesn't exist)
:CREATE
    echo "Project creation : "
    echo "-------------------"
    if exist %PROJECT_NAME% (
        echo "Project already exists..."
        call :FULL_CLEAN
    )
    if not exist %PROJECT_NAME% (
        call cordova create %PROJECT_NAME%
    )
    xcopy /y config.xml %PROJECT_NAME%\config.xml
	xcopy /y mobile_app\res\icon\favicon.png %PROJECT_NAME%\res\icon\favicon.png
goto :EOF


REM ---------------------------- DISPLAY_HELP ----------------------------
REM display help message
:DISPLAY_HELP
    echo "Mobile application script :"
    echo "Usage : mobile.bat [fullBuildAPK|updateAPK|delete]"
    echo "   fullBuildAPK : installs dependencies, build the project and creates the android APK"
    echo "   updateAPK : generate a new apk (based on a previous project created by fullBuildAPK)"
    echo "   delete : deletes the mobile project"
goto :EOF


REM -------------------------- DELETE_PROJECT -------------------------
REM create cordova project (if it doesn't exist)
:DELETE_PROJECT
    echo "Project deletion : "
    echo "-------------------"
    if exist %PROJECT_NAME% (
        rd /q /s %PROJECT_NAME%
    )
goto :EOF


REM -------------------------- DELETE_BINARIES -------------------------
REM create cordova project (if it doesn't exist)
:DELETE_BINARIES
    echo "APK deletion : "
    echo "-------------------"
    if exist APK (
        rd /q /s APK
    )
goto :EOF


REM ---------------------------- FULL_CLEAN ----------------------------
REM complete clean of the cordova project
:FULL_CLEAN
    echo "Project full clean : "
    echo "---------------------"
    call :CLEAN_PROJECT
    call :FULL_CLEAN
goto :EOF


REM ------------------------ CLEAN_PROJECT ---------------------------
REM clean the build results of the platforms
:CLEAN_PROJECT
    echo "Project clean : "
    echo "----------------"
    if exist %PROJECT_NAME% (
        cd %PROJECT_NAME%
        call %CORDOVA_BIN%\cordova clean
        cd %BASE_DIR%
    )
goto :EOF


REM ---------------------------- CLEAN_WWW ----------------------------
REM fully clean cordova's www directory (if it exists)
:CLEAN_WWW
    echo "Project www clean: "
    echo "-------------------"
    if exist %WWW_DIR% (
        del /f /q /s %WWW_DIR%
    )
goto :EOF


REM ---------------------------- COPY_WWW ----------------------------
REM copy project into cordova www folder
:COPY_WWW
    echo "Project www copy : "
    echo "-------------------"
    if exist %WWW_DIR% (
        rd /q /s %WWW_DIR%\*
        REM Saas always build -- if not exist dist ( --
            echo "Building project : "
            echo "-------------------"
            call :LAUNCH_NG_BUILD
        REM -- ) --
        xcopy /y /e /i dist %WWW_DIR%
        del /F /Q %WWW_DIR%\*.gz
    )
goto :EOF


REM ------------------------ LAUNCH_NG_BUILD ------------------------
REM Launch the app build
:LAUNCH_NG_BUILD
    if not exist node_modules (
        call :INSTALL_NODE_DEPENDENCIES
    )
    echo "Building angular project : "
    echo "---------------------------"

    call ng b --aot --prod --base-href=file:///android_asset/www/ -c=mobile
goto :EOF


REM ------------------------ INSTALL_NODE_DEPENDENCIES ------------------------
REM Install node dependencies
:INSTALL_NODE_DEPENDENCIES
    echo "Project dependencies installation : "
    echo "------------------------------------"
    npm install
goto :EOF


REM ---------------------------- ADD_PLATFORM_ANDROID ----------------------------
REM add android platform (if not already present)
:ADD_PLATFORM_ANDROID
    echo "Project android platform creation : "
    echo "------------------------------------"
    if not exist %PROJECT_NAME% (
        call :CREATE
    )
    if not exist %PROJECT_NAME%\platforms\android (
        cd %PROJECT_NAME%
        call cordova platform add android
        cd %BASE_DIR%
    )
goto :EOF


REM ---------------------------- BUILD ----------------------------
:BUILD
    echo "Project build : "
    echo "----------------"
    if exist %PROJECT_NAME% (
        cd %PROJECT_NAME%
        call cordova build
        cd %BASE_DIR%
    )
goto :EOF


REM ---------------------------- MOVE_BINARIES ----------------------------
:MOVE_BINARIES
    echo "Project binaries relocated : "
    echo "-----------------------------"
    if exist %PROJECT_NAME%\platforms\android\build\outputs\apk (
        md APK
        xcopy /y %PROJECT_NAME%\platforms\android\build\outputs\apk\* APK
    )
goto :EOF
